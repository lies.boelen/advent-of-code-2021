import pandas as pd
import numpy as np
import re

import click


def read_data(fn):
    """ specific for this task """
    signal_patterns = []
    displays = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            sp, d = line.strip().split(" | ")
            signal_patterns += [sp.split(" ")]
            displays += [d.split(" ")]
            # read new line
            line = fp.readline()
    return signal_patterns, displays


# def cost


@click.command()
@click.argument("fn")
def main(fn):
    signal_patterns, displays = read_data(fn)
    segment_counts = np.array([[len(x) for x in d] for d in displays])
    bc = np.bincount(segment_counts.flatten())
    # 1: 2 segments
    # 4: 4 segments
    # 7: 3 segments
    # 8: 7 segments
    print(bc)
    total_1478 = bc[[2, 3, 4, 7]].sum()
    print("Total of 1, 4, 7, 8 = {}".format(total_1478))


if __name__ == "__main__":
    main()