import pandas as pd
import numpy as np
import re

import click


def read_data(fn):
    """ specific for this task """
    arr = pd.read_csv(fn, header=None).values.flatten()
    # example \/
    # arr = np.array([3, 4, 3, 1, 2])
    return np.bincount(arr.astype(int), minlength=9)


def update_population(pop):
    newpop = np.zeros_like(pop)
    newpop[:-1] = pop[1:]
    newpop[-1] = pop[0]
    newpop[6] += pop[0]
    return newpop


@click.command()
@click.argument("fn")
@click.argument("ticks")
def main(fn, ticks):
    pop = read_data(fn)
    # print(pop)
    print("Day 0: {}".format(pop.sum()))
    for tick in range(int(ticks)):
        pop = update_population(pop)
        # print(pop)
        print("Day {}: {}".format(tick + 1, pop.sum()))
        # print("---")


if __name__ == "__main__":
    main()