import pandas as pd
import numpy as np

import click

CLOSER_DICT = {
    "}": "{",
    ")": "(",
    "]": "[",
    ">": "<",
}

OPENERS = list(CLOSER_DICT.values())

SCORING_DICT = {
    "}": 1197,
    ")": 3,
    "]": 57,
    ">": 25137,
    "-": 0,  # incomplete line
}


def read_data(fn):
    """ specific for this task """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return lines


def process_one_line(line):
    tracking_list = []
    for char in line:
        if char in OPENERS:
            tracking_list.append(char)
        else:
            last_opened = tracking_list.pop(-1)
            # needs to close the last opened bracket type
            if CLOSER_DICT[char] != last_opened:
                return char
    return "-"  # just an incomplete line


@click.command()
@click.argument("fn")
def main(fn):
    inputs = read_data(fn)
    corruption_outputs = [process_one_line(line) for line in inputs]
    score = np.array([SCORING_DICT[x] for x in corruption_outputs]).sum()
    print("Sum of offending characters = {}".format(score))


if __name__ == "__main__":
    main()