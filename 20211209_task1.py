import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ specific for this task """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return np.array(lines).astype(int)


def scan_map(map):
    nr, nc = map.shape
    smaller_than_right_nb = np.concatenate(
        [map[:, :-1] < map[:, 1:], np.ones((nr, 1), dtype=bool)], axis=-1
    )
    smaller_than_left_nb = np.concatenate(
        [
            np.ones((nr, 1), dtype=bool),
            map[:, 1:] < map[:, :-1],
        ],
        axis=-1,
    )
    smaller_than_top_nb = np.concatenate(
        [
            np.ones((1, nc), dtype=bool),
            map[1:, :] < map[:-1, :],
        ],
        axis=0,
    )
    smaller_than_bottom_nb = np.concatenate(
        [map[:-1, :] < map[1:, :], np.ones((1, nc), dtype=bool)], axis=0
    )
    all_smaller = (
        smaller_than_bottom_nb
        & smaller_than_top_nb
        & smaller_than_right_nb
        & smaller_than_left_nb
    )
    print(map)
    print(all_smaller)
    risk_level = ((map + 1) * all_smaller.astype(int)).sum()
    return risk_level


@click.command()
@click.argument("fn")
def main(fn):
    map = read_data(fn)
    total_risk = scan_map(map)
    print("Sum of risks = {}".format(total_risk))


if __name__ == "__main__":
    main()