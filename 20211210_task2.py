from os import kill
import pandas as pd
import numpy as np

import click

CLOSER_DICT = {
    "}": "{",
    ")": "(",
    "]": "[",
    ">": "<",
}

OPENER_DICT = {v: k for k, v in CLOSER_DICT.items()}

OPENERS = list(CLOSER_DICT.values())

SCORING_DICT = {
    "}": 3,
    ")": 1,
    "]": 2,
    ">": 4,
    "-": 0,  # corrupt line
}


def read_data(fn):
    """ specific for this task """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return lines


def process_one_line(line):
    tracking_list = []
    for char in line:
        if char in OPENERS:
            tracking_list.append(char)
        else:
            last_opened = tracking_list.pop(-1)
            # needs to close the last opened bracket type
            if CLOSER_DICT[char] != last_opened:
                return "-"
    # if you've made it this far, you've got an uncorrupted line
    return "".join([OPENER_DICT[x] for x in tracking_list[::-1]])


def get_line_score(string):
    out = 0
    for x in string:
        out = out * 5 + SCORING_DICT[x]
    return out


def get_final_score(arr):
    # drop zeros
    arr = arr[np.where(arr > 0)]
    # sort
    arr = np.sort(arr)
    l = len(arr)
    return arr[np.floor(l / 2).astype(int)]


@click.command()
@click.argument("fn")
def main(fn):
    inputs = read_data(fn)
    completed_lines = [process_one_line(line) for line in inputs]
    scores = np.array([get_line_score(x) for x in completed_lines])
    score = get_final_score(scores)
    # print(scores)
    print("Autocompletion score = {}".format(score.sum()))


if __name__ == "__main__":
    main()