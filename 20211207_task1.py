import pandas as pd
import numpy as np
import re

import click


def read_data(fn):
    """ specific for this task """
    arr = pd.read_csv(fn, header=None).values.flatten()
    # example \/
    # arr = np.array([16, 1, 2, 0, 4, 2, 7, 1, 2, 14])
    return arr


@click.command()
@click.argument("fn")
def main(fn):
    positions = read_data(fn)  # np array
    min_pos = np.min(positions)
    max_pos = np.max(positions)
    possible_destinations = np.arange(min_pos, max_pos + 1)
    possible_dist_sums = np.abs(
        possible_destinations[:, None] - positions[None, :]
    ).sum(axis=-1)
    best_destination = possible_destinations[np.argmin(possible_dist_sums)]

    print("Best destination = {}".format(best_destination))
    print("Distance to best destination = {}".format(np.min(possible_dist_sums)))


if __name__ == "__main__":
    main()