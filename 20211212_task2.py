import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ read in as list of lists of len 2 """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [line.strip().split("-")]
            # read new line
            line = fp.readline()
    return lines


def get_connections_per_node(node, connections):
    return [y for [x, y] in connections if (x == node) and (y != "start")]


def generate_connection_dict(input_data):
    nodes = np.unique(np.array(input_data).flatten()).tolist()
    # print(nodes)
    input_data_reverse = [[y, x] for [x, y] in input_data]
    # print(input_data_reverse)
    connections = input_data + input_data_reverse
    connection_dict = {
        node: get_connections_per_node(node, connections) for node in nodes
    }
    # print(connection_dict)
    return connection_dict


def get_available_nodes(from_node, available_nodes, small_cave_count_dict=None):
    if not small_cave_count_dict:
        small_cave_count_dict = {
            k: 0 for k in available_nodes if k.islower() and (k not in ["start", "end"])
        }
    else:
        small_cave_count_dict = small_cave_count_dict.copy()

    if from_node[0].islower():
        # update the visit count for this cave (not if start/end)
        if from_node in small_cave_count_dict.keys():
            small_cave_count_dict[from_node] += 1
        # check whether any small cave has been visited twice (including from_node)
        # if there has, remove from_node from the available nodes
        if (np.array(list(small_cave_count_dict.values())) > 1).any():
            # remove from node AND any other small cave node that has been visited once already
            return [
                x for x in available_nodes if small_cave_count_dict.get(x, 0) == 0
            ], small_cave_count_dict

    return available_nodes, small_cave_count_dict


def find_all_paths(
    connection_dict, from_node="start", available_nodes=None, small_cave_count_dict=None
):
    """
    available nodes: nodes which can still be used
    """
    if not available_nodes:
        # only if from_node == "start"
        available_nodes, small_cave_count_dict = get_available_nodes(
            from_node, [x for x in connection_dict.keys() if x != "start"], None
        )

    if from_node == "end":
        return [["end"]]

    # find all next possible nodes from from_node
    next_nodes = [x for x in connection_dict[from_node] if (x in available_nodes)]
    if len(next_nodes) == 0:
        return [["x"]]  # we'll still have to remove shit that doesn't end in 'end'
    else:
        return [
            [from_node, x]
            for next_node in next_nodes
            for x in find_all_paths(
                connection_dict,
                next_node,
                *get_available_nodes(next_node, available_nodes, small_cave_count_dict),
            )
        ]


def unfurl_listy_path(nested_list):
    if len(nested_list) == 1:
        return [nested_list[0]]
    else:
        return [nested_list[0]] + unfurl_listy_path(nested_list[1])


@click.command()
@click.argument("fn")
def main(fn):
    input_data = read_data(fn)
    connection_dict = generate_connection_dict(input_data)
    all_paths = [unfurl_listy_path(x) for x in find_all_paths(connection_dict)]
    all_viable_paths = [x for x in all_paths if (x[-1] == "end")]
    # for path in all_viable_paths:
    #     print(path)
    #     print("========")
    print("Total number of viable paths = {}".format(len(all_viable_paths)))


if __name__ == "__main__":
    main()