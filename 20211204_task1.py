import pandas as pd
import numpy as np
import re

import click


def read_data(fn):
    """ specific for this task """
    boards = []
    new_board = []
    with open(fn, "r") as fp:
        # first line
        line = fp.readline()
        numbers_drawn = np.array(line.split(",")).astype(int)
        line = fp.readline()
        # now onto the boards
        while line:
            if line == "\n":
                # the next line starts a new board
                # add new board to the boards list, if it exists
                if new_board != []:
                    # need clause otherwise issue with first board
                    boards += [new_board]
                    new_board = []
            else:
                # fill in the existing board
                new_board_line = re.split("\s+", line.replace("\n", "").strip())
                new_board += [new_board_line]
            # read new line
            line = fp.readline()
    return numbers_drawn, np.array(boards).astype(int)


def play_game(boards, numbers_drawn):
    total_winning = boards.shape[-1]
    mask_numbers_called = np.zeros_like(boards).astype(bool)
    for number in numbers_drawn:
        # update mask
        mask_numbers_called = mask_numbers_called | (boards == number)
        # check for winning board
        if (mask_numbers_called.sum(axis=-1) == total_winning).any():
            # find winning board
            board_idx = np.where(mask_numbers_called.sum(axis=-1) == total_winning)[0][
                0
            ]
            break
        elif (mask_numbers_called.sum(axis=1) == total_winning).any():
            # find winning board
            board_idx = np.where(mask_numbers_called.sum(axis=1) == total_winning)[0][0]
            break
    # print outcomes
    print("last number = {}".format(number))
    print("board_idx = {}".format(board_idx))
    score_winning_board = (
        boards[board_idx] * (1 - mask_numbers_called[board_idx].astype(int))
    ).sum()
    print(boards[board_idx])
    print("score of winning board = {}".format(score_winning_board))
    print("final_score = {}".format(score_winning_board * number))


@click.command()
@click.argument("fn")
def main(fn):
    numbers_drawn, boards = read_data(fn)  # get np array
    # print(numbers_drawn)
    # print(len(numbers_drawn))
    # print(boards.shape)
    play_game(boards, numbers_drawn)


if __name__ == "__main__":
    main()