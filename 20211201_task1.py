import numpy as np
import pandas as pd

import click


def read_data(fn):
    """ returns a numpy array """
    return pd.read_csv(fn, header=None).values.flatten()


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get np array
    print((dat[1:] - dat[:-1] >= 0).sum())


if __name__ == "__main__":
    main()