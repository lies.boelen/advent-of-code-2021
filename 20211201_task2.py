import numpy as np
import pandas as pd

import click


def read_data(fn):
    """ returns a numpy array """
    return pd.read_csv(fn, header=None).iloc[:, 0]


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get pd Series
    # dat = pd.Series(np.array([199, 200, 208, 210, 200, 207, 240, 269, 260, 263]))
    print(dat)
    dat = dat.rolling(window=3).sum().values[2:]
    print(dat)
    print((dat[1:] - dat[:-1] > 0).sum())


if __name__ == "__main__":
    main()