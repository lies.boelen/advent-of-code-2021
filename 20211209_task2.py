import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ specific for this task """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    arr = np.array(lines).astype(int)
    # arr[np.where(arr == 9)] = -1
    return arr


def scan_map_for_basins(map, number_of_local_mins):
    basin_map = np.zeros_like(map)
    basin_number = 10
    nr, nc = map.shape
    for i in range(nr):
        for j in range(nc):
            if map[i, j] == 9:
                # hit a nine
                pass
            # look left - can we connect
            elif (i > 0) & (map[i - 1, j] < 9):
                basin_map[i, j] = basin_map[i - 1, j]
            # look above - can we connect
            elif (j > 0) & (map[i, j - 1] < 9):
                basin_map[i, j] = basin_map[i, j - 1]
            # if we can't connect above or left, we're in a new basin
            else:
                basin_map[i, j] = basin_number
                basin_number += 1
    # this potentially misses out on right/bottom connections, so go through it again
    # and update the board
    # in fact, keep going through the basin map until no more changes have been made (while_condition)
    while_condition = True
    while while_condition:
        while_condition = False
        # keep checking until the number of basins is equal to the number of connected domains
        for i in range(nr):
            for j in range(nc):
                # if neighbour on the right has a different number and both are positive: change
                if i < nr - 1:
                    if (
                        (basin_map[i, j] > 0)
                        & (basin_map[i + 1, j] > 0)
                        & (basin_map[i, j] != basin_map[i + 1, j])
                    ):
                        val_to_replace = basin_map[i, j]
                        replacement_val = basin_map[i + 1, j]
                        basin_map[
                            np.where(basin_map == val_to_replace)
                        ] = replacement_val
                        while_condition = True
                # same but with neighbour on the left
                if (
                    (i > 0)
                    & (basin_map[i, j] > 0)
                    & (basin_map[i - 1, j] > 0)
                    & (basin_map[i, j] != basin_map[i - 1, j])
                ):
                    val_to_replace = basin_map[i, j]
                    replacement_val = basin_map[i - 1, j]
                    basin_map[np.where(basin_map == val_to_replace)] = replacement_val
                    while_condition = True
                # same but with bottom nb
                if j < nc - 1:
                    if (
                        (basin_map[i, j] > 0)
                        & (basin_map[i, j + 1] > 0)
                        & (basin_map[i, j] != basin_map[i, j + 1])
                    ):
                        val_to_replace = basin_map[i, j]
                        replacement_val = basin_map[i, j + 1]
                        basin_map[
                            np.where(basin_map == val_to_replace)
                        ] = replacement_val
                        while_condition = True
                # same with top nb
                if (
                    (j > 0)
                    & (basin_map[i, j] > 0)
                    & (basin_map[i, j - 1] > 0)
                    & (basin_map[i, j] != basin_map[i, j - 1])
                ):
                    val_to_replace = basin_map[i, j]
                    replacement_val = basin_map[i, j - 1]
                    basin_map[np.where(basin_map == val_to_replace)] = replacement_val
                    while_condition = True
        # print(while_condition)
    # print(basin_map)
    basin_sizes = np.bincount(basin_map.flatten())[1:]
    return (np.sort(basin_sizes)[-3:]).prod()


def scan_map_for_number_of_low_points(map):
    nr, nc = map.shape
    smaller_than_right_nb = np.concatenate(
        [map[:, :-1] < map[:, 1:], np.ones((nr, 1), dtype=bool)], axis=-1
    )
    smaller_than_left_nb = np.concatenate(
        [
            np.ones((nr, 1), dtype=bool),
            map[:, 1:] < map[:, :-1],
        ],
        axis=-1,
    )
    smaller_than_top_nb = np.concatenate(
        [
            np.ones((1, nc), dtype=bool),
            map[1:, :] < map[:-1, :],
        ],
        axis=0,
    )
    smaller_than_bottom_nb = np.concatenate(
        [map[:-1, :] < map[1:, :], np.ones((1, nc), dtype=bool)], axis=0
    )
    all_smaller = (
        smaller_than_bottom_nb
        & smaller_than_top_nb
        & smaller_than_right_nb
        & smaller_than_left_nb
    )
    return all_smaller.sum()


@click.command()
@click.argument("fn")
def main(fn):
    map = read_data(fn)
    number_of_local_mins = scan_map_for_number_of_low_points(map)
    print(number_of_local_mins)
    top3prod = scan_map_for_basins(map, number_of_local_mins)
    print("Product of size of three biggest basins = {}".format(top3prod))


if __name__ == "__main__":
    main()