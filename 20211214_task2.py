import pandas as pd
import numpy as np

import click
import itertools


def read_data(fn):
    """ custom for task """
    insert_instructions = []
    first_part = True
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            if first_part:
                if line != "\n":
                    initial_product = line.strip()
                else:
                    first_part = False
            else:
                insert_instructions += [line.strip().split(" -> ")]
            # read new line
            line = fp.readline()
    insert_instructions = {pattern: insert for pattern, insert in insert_instructions}
    return initial_product, insert_instructions


def find_components(product, insert_instructions):
    all_letters = (
        product
        + "".join(insert_instructions.keys())
        + "".join(insert_instructions.values())
    )
    return np.unique(np.array(list(all_letters))).tolist()


def get_all_two_letter_combo_counter(components, product):
    all_combos = np.unique(
        np.array(["".join(a) for a in itertools.product(components, components)])
    )
    out = {a: 0 for a in all_combos}
    for i in range(len(product) - 1):
        out[product[i : (i + 2)]] += 1
    return out


def expand_product(counter_dict, insert_instructions):
    new_counter_dict = counter_dict.copy()
    for pattern, insert in insert_instructions.items():
        pattern_count = counter_dict[pattern]
        # add counts for two new patterns
        new_counter_dict[pattern[0] + insert] += pattern_count
        new_counter_dict[insert + pattern[1]] += pattern_count
        # remove original pattern
        new_counter_dict[pattern] -= pattern_count
    return new_counter_dict


def count_components(counter_dict, components, product):
    out = {
        component: np.array(
            [counter_dict[x] for x in counter_dict.keys() if component == x[0]]
            + [counter_dict[x] for x in counter_dict.keys() if component == x[1]]
        ).sum()
        / 2
        for component in components
    }
    out[product[0]] += 0.5
    out[product[-1]] += 0.5
    return out


@click.command()
@click.argument("fn")
def main(fn):
    product, insert_instructions = read_data(fn)
    components = find_components(product, insert_instructions)
    counter_dict = get_all_two_letter_combo_counter(components, product)
    n_rounds = 40
    for round in range(n_rounds):
        # print(round)
        counter_dict = expand_product(counter_dict, insert_instructions)
        # if round == 1:
        # print(counter_dict)
    # print(counter_dict)
    component_count = count_components(counter_dict, components, product)
    print(component_count)
    count_values = np.array(list(component_count.values()))
    print("Diff = {}".format(count_values.max() - count_values.min()))


if __name__ == "__main__":
    main()