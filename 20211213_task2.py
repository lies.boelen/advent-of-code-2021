import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ custom for task """
    initial_positions = []
    fold_instructions = []
    ip = True
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            if ip:
                if line != "\n":
                    initial_positions += [np.array(line.strip().split(",")).astype(int)]
                else:
                    ip = False
            else:
                fold_instructions += [
                    line.strip().replace("fold along ", "").split("=")
                ]
            # read new line
            line = fp.readline()
    return np.array(initial_positions), fold_instructions


def build_page(positions):
    max_x = np.max(positions[:, 0])
    max_y = np.max(positions[:, 1])
    # print(max_x, max_y)
    page = np.zeros((max_y + 1, max_x + 1), dtype=int)
    page[positions[:, 1], positions[:, 0]] = 1
    return page


def print_page(page):
    to_print = page.astype(int).astype(str)
    for line in to_print:
        print("".join(line).replace("0", " ").replace("1", "#"))


def fold_page(page, fold_along, fold_number):
    if fold_along == "y":
        # fold up
        area_to_fold_up = page[(fold_number + 1) :]
        area_after_folding_up = area_to_fold_up[::-1]
        area_to_fold_onto = page[:fold_number]
        area_to_fold_onto[-area_to_fold_up.shape[0] :] += area_after_folding_up
        return np.minimum(area_to_fold_onto, 1)
    else:
        # fold left
        area_to_fold_left = page[:, (fold_number + 1) :]
        area_after_folding_left = area_to_fold_left[:, ::-1]
        area_to_fold_onto = page[:, :fold_number]
        area_to_fold_onto[:, -area_to_fold_left.shape[1] :] += area_after_folding_left
        return np.minimum(area_to_fold_onto, 1)


@click.command()
@click.argument("fn")
def main(fn):
    initial_positions, fold_instructions = read_data(fn)
    # build page
    page = build_page(initial_positions)
    # start folding
    for fold_along, fold_number in fold_instructions:
        page = fold_page(page, fold_along, int(fold_number))
    print_page(page)


if __name__ == "__main__":
    main()