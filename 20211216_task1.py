import pandas as pd
import numpy as np

from IPython import embed

import click


HEX_TO_BIN_DICT = {
    "0": "0000",
    "1": "0001",
    "2": "0010",
    "3": "0011",
    "4": "0100",
    "5": "0101",
    "6": "0110",
    "7": "0111",
    "8": "1000",
    "9": "1001",
    "A": "1010",
    "B": "1011",
    "C": "1100",
    "D": "1101",
    "E": "1110",
    "F": "1111",
}


class VersionedInt(object):
    def __init__(self, version, value):
        self._version = version
        self._value = value

    def __str__(self):
        return str(self._value)

    def __repr__(self):
        return str([self._version, self._value])

    def _get_version(self):
        return self._version


def process_literal_value(bin_string):
    while_cond = True
    bin_value_str = ""
    while while_cond:
        while_cond = bool(int(bin_string[0]))
        bin_value_str += bin_string[1:5]
        bin_string = bin_string[5:]
    value = bin_to_dec(bin_value_str)
    return value, bin_string


def build_tree(bin_string):
    """tree is one big operator for sure"""
    return process_bin_string(bin_string)[0]


def process_bin_string(bin_string):
    out = []
    while (len(bin_string) > 0) & (bin_to_dec(bin_string) > 0):
        version = bin_to_dec(bin_string[:3])
        type_id = bin_to_dec(bin_string[3:6])
        bin_string = bin_string[6:]
        if type_id == 4:
            # literal
            value, bin_string = process_literal_value(bin_string)
            out += [VersionedInt(version, value)]
        else:
            # operator -> new tree
            baby_tree = StupidTree(version, type_id)
            bin_string = baby_tree.add_children_from_bin_str(bin_string)
            out += [baby_tree]
    return out


def process_bin_string_with_leftover(bin_string):
    version = bin_to_dec(bin_string[:3])
    type_id = bin_to_dec(bin_string[3:6])
    bin_string = bin_string[6:]
    if type_id == 4:
        # literal
        value, bin_string = process_literal_value(bin_string)
        return VersionedInt(version, value), bin_string
    else:
        # operator -> new tree
        baby_tree = StupidTree(version, type_id)
        bin_string = baby_tree.add_children_from_bin_str(bin_string)
        return baby_tree, bin_string


class StupidTree(object):
    def __init__(self, version, type_id, children=[]):
        self._version = version
        self._type_id = type_id
        self._children = children

    def add_children_from_bin_str(self, bin_string):
        # length id
        length_type_id = bin_string[0]
        bin_string = bin_string[1:]
        if length_type_id == "0":
            total_bit_length = bin_to_dec(bin_string[:15])
            bin_string = bin_string[15:]
            subpackets_string = bin_string[:total_bit_length]
            bin_string = bin_string[total_bit_length:]
            # process the subpackets string
            children = process_bin_string(subpackets_string)
        else:
            number_of_subpackets = bin_to_dec(bin_string[:11])
            bin_string = bin_string[11:]
            children = []
            for _ in range(number_of_subpackets):
                one_child, bin_string = process_bin_string_with_leftover(bin_string)
                children += [one_child]
        self._children = children
        return bin_string

    def _update_children(self, new_children):
        self._children = new_children

    def look_at_children(self):
        print("type children = {}".format(type(self._children)))
        for i, x in self._children:
            print("type child {} = {}".format(i, type(x)))

    def __repr__(self):
        return str(
            [self._version, self._type_id, [x.__repr__() for x in self._children]]
        )

    def _get_children(self):
        return self._children

    def _get_version(self):
        out = self._version
        for child in self._children:
            out += child._get_version()
        return out


def read_data(fn):
    """ returns hex string """
    with open(fn, "r") as fp:
        return fp.readline().strip()


def hex_to_bin(hex_str):
    return "".join([HEX_TO_BIN_DICT[x] for x in hex_str])


def bin_to_dec(bin_string):
    out = 0
    for k, l in enumerate(bin_string[::-1]):
        out += int(l) * 2 ** k
    return out


def dec_to_bin(integer, pad_to=None):
    # TODO: this if probably not needed
    if integer == 0:
        return "0"

    # find biggest power of 2 needed
    max_power = 0
    while True:
        if 2 ** (max_power + 1) > integer:
            break
        max_power += 1

    # make binary string
    out = ""
    for k in np.arange(0, max_power + 1)[::-1]:
        quot, rest = np.divmod(integer, 2 ** k)
        out += str(quot)
        integer -= quot * 2 ** k

    if pad_to:
        curr_len = len(out)
        if curr_len < pad_to:
            return "".join(["0"] * (pad_to - curr_len)) + out

    return out


@click.command()
@click.argument("fn")
def main(fn):
    hex_string = read_data(fn)
    # # example 1
    # hex_string = "D2FE28"
    # bin_string = hex_to_bin(hex_string)
    # bin_string = "111" + "111" + "0" + dec_to_bin(len(bin_string), 15) + bin_string

    # # example 2
    # hex_string = "38006F45291200"

    # # example 3
    # hex_string = "EE00D40C823060"

    # # example 4
    # hex_string = "8A004A801A8002F478"

    # # example 5
    # hex_string = "620080001611562C8802118E34"

    # example 6
    # hex_string = "C0015000016115A2E0802F182340"

    # example 7
    # hex_string = "A0016C880162017C3686B18A3D4780"

    bin_string = hex_to_bin(hex_string)
    p = build_tree(bin_string)
    print(p._get_version())


if __name__ == "__main__":
    main()