import pandas as pd
import numpy as np
import re

import click


def read_data(fn):
    """ specific for this task """
    boards = []
    new_board = []
    with open(fn, "r") as fp:
        # first line
        line = fp.readline()
        numbers_drawn = np.array(line.split(",")).astype(int)
        line = fp.readline()
        # now onto the boards
        while line:
            if line == "\n":
                # the next line starts a new board
                # add new board to the boards list, if it exists
                if new_board != []:
                    # need clause otherwise issue with first board
                    boards += [new_board]
                    new_board = []
            else:
                # fill in the existing board
                new_board_line = re.split("\s+", line.replace("\n", "").strip())
                new_board += [new_board_line]
            # read new line
            line = fp.readline()
    return numbers_drawn, np.array(boards).astype(int)


def find_last_winnig_board(boards, numbers_drawn):
    board_winner_indicator = np.zeros(boards.shape[0], dtype=bool)
    total_winning = boards.shape[-1]
    mask_numbers_called = np.zeros_like(boards).astype(bool)
    # last_winning_board_idx = -1
    for number in numbers_drawn:
        # update mask
        mask_numbers_called = mask_numbers_called | (boards == number)
        # check for winning boards
        board_winner_indicator = (
            board_winner_indicator
            | ((mask_numbers_called.sum(axis=-1) == total_winning).any(axis=-1))
            | ((mask_numbers_called.sum(axis=1) == total_winning).any(axis=-1))
        )
        print(number, np.bincount(board_winner_indicator.astype(int)))
        # check for one board remaining
        if board_winner_indicator.sum() == len(board_winner_indicator) - 1:
            last_winning_board_idx = np.where(~board_winner_indicator)[0][0]
            print("last winning board = {}".format(last_winning_board_idx))
        if board_winner_indicator.sum() == len(board_winner_indicator):
            break

    # print outcomes
    score_last_winning_board = (
        boards[last_winning_board_idx]
        * (1 - mask_numbers_called[last_winning_board_idx].astype(int))
    ).sum()
    print(boards[last_winning_board_idx])
    # print(boards)
    print("score of winning board = {}".format(score_last_winning_board))
    print("final_score = {}".format(score_last_winning_board * number))


@click.command()
@click.argument("fn")
def main(fn):
    numbers_drawn, boards = read_data(fn)  # get np array
    # print(numbers_drawn)
    # print(len(numbers_drawn))
    # print(boards.shape)
    find_last_winnig_board(boards, numbers_drawn)


if __name__ == "__main__":
    main()