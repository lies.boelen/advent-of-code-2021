import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ custom for task """
    insert_instructions = []
    first_part = True
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            if first_part:
                if line != "\n":
                    initial_product = line.strip()
                else:
                    first_part = False
            else:
                insert_instructions += [line.strip().split(" -> ")]
            # read new line
            line = fp.readline()
    insert_instructions = {pattern: insert for pattern, insert in insert_instructions}
    return initial_product, insert_instructions


def get_insert_positions(product, pattern):
    product_length = len(product)
    pattern_length = len(pattern)
    # find all occurrences of the first letter of the pattern
    first_letter_occurences = np.where(np.array(list(product)) == pattern[0])[0]
    positions = first_letter_occurences.tolist()
    # then check whether the rest of the substring matches
    for spot in first_letter_occurences:
        if (spot + pattern_length > product_length) | (
            product[spot : (spot + pattern_length)] != pattern
        ):
            positions.remove(spot)
    return positions


def insert_letter(product, letter, position):
    return product[:position] + letter + product[position:]


def expand_product(product, insert_instructions):
    letters_to_insert = {
        insert_position + 1: insert_letter
        for pattern, insert_letter in insert_instructions.items()
        for insert_position in get_insert_positions(product, pattern)
    }
    # go through these positions from the back
    insert_positions = np.sort(np.array(list(letters_to_insert.keys())).astype(int))[
        ::-1
    ]
    for ip in insert_positions:
        product = insert_letter(product, letters_to_insert[ip], ip)
    return product


def find_components(product, insert_instructions):
    all_letters = (
        product
        + "".join(insert_instructions.keys())
        + "".join(insert_instructions.values())
    )
    return np.unique(np.array(list(all_letters))).tolist()


def count_components(product, components):
    out = {c: 0 for c in components}
    for p in product:
        out[p] += 1
    return out


@click.command()
@click.argument("fn")
def main(fn):
    product, insert_instructions = read_data(fn)
    components = find_components(product, insert_instructions)
    # print(product)
    # print(insert_instructions)
    n_rounds = 10
    for round in range(n_rounds):
        product = expand_product(product, insert_instructions)
    # print(product)
    component_count = count_components(product, components)
    print(component_count)
    count_values = np.array(list(component_count.values()))
    print("Diff = {}".format(count_values.max() - count_values.min()))


if __name__ == "__main__":
    main()