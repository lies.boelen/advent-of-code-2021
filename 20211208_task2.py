import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ specific for this task """
    signal_patterns = []
    displays = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            sp, d = line.strip().split(" | ")
            signal_patterns += [sp.split(" ")]
            displays += [d.split(" ")]
            # read new line
            line = fp.readline()
    return signal_patterns, displays


def solve_line(signal_pattern, display):
    translation_dict = get_translation_dict(signal_pattern)
    REF = "abcdefg"  # make sure that order is alphabetical
    res = 0
    for i, d in enumerate(display[::-1]):
        res += translation_dict["".join([x for x in REF if x in d])] * 10 ** i
    return res


def get_translation_dict(signal_pattern):
    """
    signal_pattern is a list of length 10
    """
    number_to_string = {}
    string_to_number = {}
    REF = "abcdefg"  # make sure that order is alphabetical
    # add 1, 4, 7, 8 based on length
    for number, l in [[1, 2], [7, 3], [4, 4], [8, 7]]:
        # add to translation dict
        val = [x for x in signal_pattern if len(x) == l][0]
        val_ = "".join([x for x in REF if x in val])
        number_to_string[number] = val_
        string_to_number[val_] = number
        signal_pattern.remove(val)

    # find 6: based on 8\1 and length 6
    eight_without_one = [x for x in number_to_string[8] if x not in number_to_string[1]]
    val = [
        x
        for x in signal_pattern
        if np.array([y in x for y in eight_without_one]).all() and (len(x) == 6)
    ][0]
    val_ = "".join([x for x in REF if x in val])
    number_to_string[6] = val_
    string_to_number[val_] = 6
    signal_pattern.remove(val)
    # find 5: based on 4\1 and length 5
    four_without_one = [x for x in number_to_string[4] if x not in number_to_string[1]]
    val = [
        x
        for x in signal_pattern
        if np.array([y in x for y in four_without_one]).all() and (len(x) == 5)
    ][0]
    val_ = "".join([x for x in REF if x in val])
    number_to_string[5] = val_
    string_to_number[val_] = 5
    signal_pattern.remove(val)
    # find 9: based on 4 + 7 and len 6
    four_and_seven = number_to_string[4] + number_to_string[7]
    val = [
        x
        for x in signal_pattern
        if np.array([y in x for y in four_and_seven]).all() and (len(x) == 6)
    ][0]
    val_ = "".join([x for x in REF if x in val])
    number_to_string[9] = val_
    string_to_number[val_] = 9
    signal_pattern.remove(val)
    # find 0: only one left with len 6
    val = [x for x in signal_pattern if len(x) == 6][0]
    val_ = "".join([x for x in REF if x in val])
    number_to_string[0] = val_
    string_to_number[val_] = 0
    signal_pattern.remove(val)
    # find 2: based on 6/5 and length 5 (although all len 5 now)
    six_without_five = [x for x in number_to_string[6] if x not in number_to_string[5]]
    val = [
        x
        for x in signal_pattern
        if np.array([y in x for y in six_without_five]).all() and (len(x) == 5)
    ][0]
    val_ = "".join([x for x in REF if x in val])
    number_to_string[2] = val_
    string_to_number[val_] = 2
    signal_pattern.remove(val)
    # remaining value is 3
    val = signal_pattern[0]
    val_ = "".join([x for x in REF if x in val])
    number_to_string[3] = val_
    string_to_number[val_] = 3
    return string_to_number


@click.command()
@click.argument("fn")
def main(fn):
    signal_patterns, displays = read_data(fn)
    # example \/
    # signal_patterns = [
    #     [
    #         "acedgfb",
    #         "cdfbe",
    #         "gcdfa",
    #         "fbcad",
    #         "dab",
    #         "cefabd",
    #         "cdfgeb",
    #         "eafb",
    #         "cagedb",
    #         "ab",
    #     ]
    # ]
    # displays = [["cdfeb", "fcadb", "cdfeb", "cdbaf"]]
    # solve line by line
    displayed_numbers = np.array(
        [solve_line(code, disp) for code, disp in zip(signal_patterns, displays)]
    )
    print("Sum of codes = {}".format(displayed_numbers.sum()))


if __name__ == "__main__":
    main()