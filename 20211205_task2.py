import pandas as pd
import numpy as np
import re

import click


def redo_board(arr, new_size):
    print("reshaping")
    new_arr = np.zeros((new_size, new_size), dtype=int)
    new_arr[: arr.shape[0], : arr.shape[1]] = arr
    return new_size, new_arr


def make_seafloor_map(fn):
    """ specific for this task """
    current_size = 1000
    seafloor = np.zeros((current_size, current_size), dtype=int)
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            start, end = [
                np.array(x.split(",")).astype(int) for x in line.split(" -> ")
            ]
            # print(start, end)
            # change board size if any of the numbers is bigger than current size
            if np.max(start) > current_size - 1:
                current_size, seafloor = redo_board(seafloor, np.max(start) + 10)
            if np.max(end) > current_size - 1:
                current_size, seafloor = redo_board(seafloor, np.max(end) + 10)
            if start[0] == end[0]:
                sum_old = seafloor.sum()
                # horizontal line
                # print(seafloor.sum())
                others = np.sort(np.array([start[1], end[1]]))
                diff = others[1] - others[0] + 1
                # print(others)
                seafloor[start[0], others[0] : (others[1] + 1)] += 1
                # print(seafloor.sum())
                assert seafloor.sum() == sum_old + diff, (sum_old, diff, seafloor.sum())
            elif start[1] == end[1]:
                # vertical line
                # print(seafloor.sum())
                sum_old = seafloor.sum()
                others = np.sort(np.array([start[0], end[0]]))
                # print(others)
                diff = others[1] - others[0] + 1
                seafloor[others[0] : (others[1] + 1), start[1]] += 1
                # print(seafloor.sum())
                assert seafloor.sum() == sum_old + diff, (sum_old, diff, seafloor.sum())
            else:
                # 45 degree diagonal
                seafloor[
                    np.arange(
                        start[0],
                        end[0] + np.sign(end[0] - start[0]),
                        np.sign(end[0] - start[0]),
                    ),
                    np.arange(
                        start[1],
                        end[1] + np.sign(end[1] - start[1]),
                        np.sign(end[1] - start[1]),
                    ),
                ] += 1
            # read new line
            line = fp.readline()
    return seafloor


@click.command()
@click.argument("fn")
def main(fn):
    seafloor_map = make_seafloor_map(fn)  # get np array
    # print(seafloor_map[:10, :10])
    print((seafloor_map > 1).sum())


if __name__ == "__main__":
    main()