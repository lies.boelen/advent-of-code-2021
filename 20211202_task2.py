import click


@click.command()
@click.argument("fn")
def main(fn):
    depth, hor_pos, aim = 0, 0, 0
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            dir, size = line.split(" ")
            if dir == "down":
                aim += int(size)
            elif dir == "up":
                aim -= int(size)
            else:  # forward
                hor_pos += int(size)
                depth += aim * int(size)
            # read new line
            line = fp.readline()
    print(hor_pos, depth)
    print(hor_pos * depth)


if __name__ == "__main__":
    main()