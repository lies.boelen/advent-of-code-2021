import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ returns a 2D numpy array """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return np.array(lines, dtype=int)


# def bool_array_to_bin_string(bool_array):
#     return "".join(bool_array.astype(int).astype(str))


def bin_str_to_dec_number(bin_str):
    rev_str = bin_str[::-1]
    value = 0
    for pow, coeff in enumerate(rev_str):
        value += int(coeff) * 2 ** pow
    return value


def select_most(counts):
    if counts.min() == counts.max():
        # tie breaker
        return 1
    return np.argmax(counts)


def select_least(counts):
    if counts.min() == counts.max():
        # tie breaker
        return 0
    return np.argmin(counts)


def find_rating(arr, selection_function):
    nr, nc = arr.shape
    mask = np.ones(nr, dtype=bool)
    col_idx = 0
    while mask.sum() > 1:
        # find the bit criterion
        counts = np.bincount(arr[mask, col_idx], minlength=2)
        bit_criterion = selection_function(counts)
        # update the mask
        mask = mask & (arr[:, col_idx] == bit_criterion)
        # update the col_idx
        col_idx += 1
    # potential todo: check whether we've run out of cols too?
    return bin_str_to_dec_number("".join(arr[mask].flatten().astype(str)))


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get np array
    oxygen_generator_rating = find_rating(dat, select_most)
    co2_scrubber_rating = find_rating(dat, select_least)
    print(oxygen_generator_rating, co2_scrubber_rating)
    print(oxygen_generator_rating * co2_scrubber_rating)


if __name__ == "__main__":
    main()