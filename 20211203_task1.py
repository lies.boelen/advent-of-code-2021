import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ returns a 2D numpy array """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return np.array(lines, dtype=int)


def bool_array_to_bin_string(bool_array):
    return "".join(bool_array.astype(int).astype(str))


def bin_str_to_dec_number(bin_str):
    rev_str = bin_str[::-1]
    value = 0
    for pow, coeff in enumerate(rev_str):
        value += int(coeff) * 2 ** pow
    return value


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get np array
    # shape of the input
    nr, _ = dat.shape
    # sum of the columns
    ones_per_column = dat.sum(axis=0)
    zeros_per_column = nr - ones_per_column
    # gamma rate
    gamma_rate_bool = ones_per_column > zeros_per_column
    gamma_rate_str = bool_array_to_bin_string(gamma_rate_bool)
    gamma_rate_dec = bin_str_to_dec_number(gamma_rate_str)
    # epsilon rate
    epsilon_rate_str = bool_array_to_bin_string(~gamma_rate_bool)
    epsilon_rate_dec = bin_str_to_dec_number(epsilon_rate_str)
    print(gamma_rate_dec, epsilon_rate_dec)
    print(gamma_rate_dec * epsilon_rate_dec)


if __name__ == "__main__":
    main()