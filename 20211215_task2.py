import pandas as pd
import numpy as np

from IPython import embed

import scipy.sparse as sps

import click


def read_data(fn):
    """ returns a 2D numpy array """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return np.array(lines, dtype=int)


def extend_map(map, times=5):
    # row per row
    for r in range(times):
        if r == 0:
            arr_list = [map]
            map_ = map.copy()
            for _ in range(1, times):
                map_ = np.where(map_ == 9, 1, map_ + 1)
                arr_list += [map_]
            rows = [np.concatenate(arr_list, axis=1)]
        else:
            arr_list = arr_list[1:]
            map_ = np.where(map_ == 9, 1, map_ + 1)
            arr_list += [map_]
            rows += [np.concatenate(arr_list, axis=1)]
    return np.concatenate(rows, axis=0)


def get_unvisited_neighbours(position_2d, visited, adj_matrix):
    pos_1d = translate_2d_to_1d(position_2d[0], position_2d[1], visited.shape[-1])
    neighbours_2d = adj_matrix[pos_1d].toarray().reshape(visited.shape)
    return (1 - visited) * neighbours_2d


def find_unvisited_node_min_score(tentative_distances, visited):
    a = np.where(visited, np.inf, tentative_distances)
    xs, ys = np.where(a == np.amin(a))
    ind = 0
    # I thought the lines below would make the algorithm faster but they don't
    # # addition: choose those closer (in step count) to the end goal
    # sums = xs + ys
    # ind = np.where(sums == np.amax(sums))[0][0]
    return np.array([xs[ind], ys[ind]])


def dijkstra(map, adj_matrix):
    nr, nc = map.shape
    # n_positions = nr * nc
    visited = np.zeros_like(map, dtype=bool)
    tentative_distances = np.ones_like(map) * (np.inf)
    # define start and end points
    current_position_2d = np.array([0, 0])
    visited[tuple(current_position_2d)] = 1
    tentative_distances[tuple(current_position_2d)] = 0
    end_point_coords = np.array([nr - 1, nc - 1])
    k = 1
    while True:
        current_tentative_distance = tentative_distances[tuple(current_position_2d)]
        # break if current node is end point
        if (current_position_2d == end_point_coords).all():
            # print("here!")
            print(tentative_distances)
            return current_tentative_distance
        # collect all unvisited neighbours on 2D grid
        # print(visited)
        unvisited_neighbours = get_unvisited_neighbours(
            current_position_2d, visited, adj_matrix
        )
        # print(unvisited_neighbours)
        # if distance of current + distance between current and unvisited neighbour < tentative distance of univisited neighbour, replace
        un_pos_i, un_pos_j = np.where(unvisited_neighbours)
        for i, j in zip(un_pos_i, un_pos_j):
            if current_tentative_distance + map[i, j] < tentative_distances[i, j]:
                tentative_distances[i, j] = current_tentative_distance + map[i, j]
        # print(tentative_distances)
        # mark current node as visited
        visited[tuple(current_position_2d)] = True
        # update current node to the unvisited node with the smallest tentative distance
        current_position_2d = find_unvisited_node_min_score(
            tentative_distances, visited
        )

        # k just here for reporting
        if np.mod(k, 50) == 0:
            print(
                k,
                int(visited.sum()),
                int(tentative_distances[tuple(current_position_2d)]),
            )
            # print(visited.sum())
            # print("---")

        k += 1


def translate_2d_to_1d(i, j, nc):
    return nc * i + j


def get_adjacency_matrix(map):
    nr, nc = map.shape
    n_positions = nr * nc
    left_neighbours = np.ones(n_positions, dtype=int) * (-1)
    right_neighbours = np.ones(n_positions, dtype=int) * (-1)
    bottom_neighbours = np.ones(n_positions, dtype=int) * (-1)
    top_neighbours = np.ones(n_positions, dtype=int) * (-1)
    for i in range(nr):
        for j in range(nc):
            ij_pos = translate_2d_to_1d(i, j, nc)
            if i > 0:
                top_neighbours[ij_pos] = translate_2d_to_1d(i - 1, j, nc)
            if j > 0:
                left_neighbours[ij_pos] = translate_2d_to_1d(i, j - 1, nc)
            if i < nr - 1:
                bottom_neighbours[ij_pos] = translate_2d_to_1d(i + 1, j, nc)
            if j < nc - 1:
                right_neighbours[ij_pos] = translate_2d_to_1d(i, j + 1, nc)
    left_v2 = [
        (x, y) for x, y in zip(np.arange(n_positions), left_neighbours) if y > -1
    ]
    right_v2 = [
        (x, y) for x, y in zip(np.arange(n_positions), right_neighbours) if y > -1
    ]
    top_v2 = [(x, y) for x, y in zip(np.arange(n_positions), top_neighbours) if y > -1]
    bottom_v2 = [
        (x, y) for x, y in zip(np.arange(n_positions), bottom_neighbours) if y > -1
    ]
    r = np.concatenate(
        [
            np.array([x[0] for x in left_v2]),
            np.array([x[0] for x in right_v2]),
            np.array([x[0] for x in top_v2]),
            np.array([x[0] for x in bottom_v2]),
        ]
    )
    c = np.concatenate(
        [
            np.array([x[1] for x in left_v2]),
            np.array([x[1] for x in right_v2]),
            np.array([x[1] for x in top_v2]),
            np.array([x[1] for x in bottom_v2]),
        ]
    )
    return sps.coo_matrix(
        (np.ones(len(r)), (r, c)), shape=(n_positions, n_positions)
    ).tocsr()


@click.command()
@click.argument("fn")
def main(fn):
    map = read_data(fn)
    map = extend_map(map)
    print(map.shape)
    adjacency_matrix = get_adjacency_matrix(map)
    bla = dijkstra(map, adjacency_matrix)
    print("Risk along safest route = {}".format(int(bla)))
    # embed()


if __name__ == "__main__":
    main()