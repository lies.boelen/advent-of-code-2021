import pandas as pd
import numpy as np

import click


def read_data(fn):
    """ returns a 2D numpy array """
    lines = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            lines += [list(line.strip())]
            # read new line
            line = fp.readline()
    return np.array(lines, dtype=int)


@click.command()
@click.argument("fn")
def main(fn):
    energy_levels = read_data(fn)  # get np array
    n_ticks = 100
    has_blinked = np.zeros(
        (n_ticks, energy_levels.shape[0], energy_levels.shape[1]), dtype=bool
    )
    for tick in range(n_ticks):
        # increase the energy levels by one
        energy_levels += 1
        # start blinking
        will_blink = energy_levels > 9
        # print(will_blink)
        while will_blink.sum() > 0:
            # register blink
            has_blinked[tick] = has_blinked[tick] | will_blink
            # add 1 to neighbours' energy
            energy_levels[:-1] += will_blink[1:]  # N
            energy_levels[1:] += will_blink[:-1]  # S
            energy_levels[:, :-1] += will_blink[:, 1:]  # W
            energy_levels[:, 1:] += will_blink[:, :-1]  # E
            energy_levels[:-1, 1:] += will_blink[1:, :-1]  # NE
            energy_levels[:-1, :-1] += will_blink[1:, 1:]  # NW
            energy_levels[1:, 1:] += will_blink[:-1, :-1]  # SE
            energy_levels[1:, :-1] += will_blink[:-1, 1:]  # SW
            # update while condition
            will_blink = (~has_blinked[tick]) & (energy_levels > 9)
        # all blinkers -> 0
        energy_levels[np.where(has_blinked[tick])] = 0
    print("number of flashes = {}".format(has_blinked.sum()))


if __name__ == "__main__":
    main()