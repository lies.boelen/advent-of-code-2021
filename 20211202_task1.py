import numpy as np
import pandas as pd

import click


def read_data(fn):
    """ returns a numpy array """
    dat = pd.read_csv(fn, header=None, sep=" ")
    dat.columns = ["direction", "size"]
    return dat


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get df
    agg_dat = dat.groupby("direction").agg({"size": "sum"})
    print(agg_dat)
    hor_pos = np.abs(agg_dat.loc["forward"])
    vert_pos = agg_dat.loc["down"] - agg_dat.loc["up"]
    print(hor_pos, vert_pos)
    print(hor_pos * vert_pos)


if __name__ == "__main__":
    main()